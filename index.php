<?php 
require_once("connect.php");
$stmt=$conn->prepare("SELECT * FROM records ");//выбрать все стркои из базы данных
$stmt->execute();
$result=$stmt->get_result(); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Подключаем Bootstrap  к проекту -->
    <link rel="stylesheet" a href="CSS/bootstrap.css"/> 
    <title>View data</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <!-- Jquery Library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


</head>
<body class="bg-dark">
    <div class="container">
        <div class="row">
            <div class="col m-auto">
                <div class="card mt-5">
                <h3 class="bg-success text while text-center py-3">List of Users</h3>
                <div class="form-inline pb-2 pt-2">
                <label for="search" class="font-weight-bold lead text-dark ml-2">Search Data</label>&nbsp; &nbsp; &nbsp;
                <input type="text" name="search" id="search_text" class="form-control form-control-lg-rounded-0 boreder-primary" placeholder="Search..">
                
            </div>
                
                   <table class="table table-hover table-light table-striped table-bordered" id="table-data">
                   <thead>
                       <tr>
                           <th>User ID</th>
                           <th>User Name</th>
                           <th>User Email</th>
                           <th>User Age</th>
                           <th>Edit</th>
                           <th>Delete</th>
                           <th>Append <a href="append.php"><img src="img/add-icon.png" height="27px" alt="add-icon"></a></th>
                       </tr>
                    </thead>
                       <?php 
                       while($row=mysqli_fetch_assoc($result))
                       {
                           $UserID = $row['User_ID'];
                           $UserName= $row['User_Name'];
                           $UserEmail =$row['User_Email'];
                           $UserAge = $row['User_Age'];
                       ?>
                       <tbody>
                       <tr>
                           <td > <?php echo $UserID?></td>
                           <td> <?php echo $UserName?></td>
                           <td> <?php echo $UserEmail?></td>
                           <td> <?php echo $UserAge?></td>
                           <td><a href="edit.php?ID=<?php echo $UserID?>"><img src="img/84380.png" height="30px" alt="edit-icon"></a></td>
                           <td><a href="delete.php?Del=<?php echo $UserID?>"><img src="img/remove.png" height="30px" alt="remove-icon"></a></td>
                        </tr>
                        <?php
                       }
                       ?>
                        </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript"> 
        $("#search_text").on("keyup",function()
        {
            var value=$(this).val();
            $("table tr").each(function(result)
            {
                if(result!==0)
                {
                    var id=$(this).find("td:eq(0)").text();
                    var name=$(this).find("td:eq(1)").text();
                    var email=$(this).find("td:eq(2)").text();
                    var age=$(this).find("td:eq(3)").text();
                    if(id.indexOf(value)!==0 && id.toLowerCase().indexOf(value.toLowerCase())<0 
                    && name.indexOf(value)!==0 && name.toLowerCase().indexOf(value.toLowerCase())<0
                     && email.indexOf(value)!==0 && email.toLowerCase().indexOf(value.toLowerCase())<0
                      && age.indexOf(value)!==0 && age.toLowerCase().indexOf(value.toLowerCase())<0)
                    {
                        $(this).hide();
                    }
                    else{
                        $(this).show();
                    }
                }
            });
        });
    </script>
</body>
</html>