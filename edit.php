<?php 
require_once("connect.php");
$UserID = $_GET['ID'];
$query = " select * from records where User_ID='".$UserID."'";
$result = mysqli_query($conn,$query);
while($row=mysqli_fetch_assoc($result))
{
    $UserID = $row['User_ID']; 
    $UserName= $row['User_Name'];
    $UserEmail =$row['User_Email'];
    $UserAge = $row['User_Age'];
}   
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Подключаем Bootstrap  к проекту -->
    <link rel="stylesheet" a href="CSS/bootstrap.css"/> 
    <title>Add a Data</title>
</head>
<body class="bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 m-auto">
                <div class="card mt-5">
                    <div class="card-title">
                        <h3 class="bg-success text while text-center py-3">Update Form</h3>
                    </div>
                    <div class="card-body">
                        <form action="update.php?ID=<?php echo $UserID ?>" method="post" autocomplete="on">
                        <input type="text" class="form-control mb-3" placeholder="User_name"  name="name"  value="<?php echo $UserName ?>">
                        <input type="email" class="form-control mb-3" placeholder="User_email"  name="email" value="<?php echo $UserEmail ?>">
                        <input type="number" class="form-control mb-3" placeholder="User_age"  name="age" value="<?php echo $UserAge?>">
                        <button class="btn btn-primary" name="update">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
