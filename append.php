<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Подключаем Bootstrap  к проекту -->
    <link rel="stylesheet" a href="CSS/bootstrap.css"/> 
    <title>Add a Data</title>
</head>
<body class="bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 m-auto">
                <div class="card mt-5">
                    <div class="card-title">
                        <h3 class="bg-success text while text-center py-3">Registration Form </h3>
                    </div>
                    <div class="card-body">
                        <form action="insert.php" method="post" autocomplete="on">
                        <input type="text" class="form-control mb-3" placeholder="User_name" required name="name">
                        <input type="email" class="form-control mb-3" placeholder="User_email" required name="email">
                        <input type="number" class="form-control mb-3" placeholder="User_age" required name="age">
                        <button class="btn btn-primary" name="submit">Confirm</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>